import React from "react";
import { Route, HashRouter, Switch } from "react-router-dom";
import ProfileList from "./components/profileList/ProfileList.component";
import SignIn from "./components/sign-in/SignIn.component";
import ForgotPassword from "./components/forgotPassword/ForgotPassword.component";
import ScrollToTop from "./components/common/ScrollTop";
import MenuAppBar from "./components/global/header/header.component";

export default props => (
  <HashRouter>
    <ScrollToTop>
      <MenuAppBar />
      <Switch>
        <Route exact path="/" component={ProfileList} />
        <Route exact path="/login" component={SignIn} />
        <Route exact path="/forgotPassword" component={ForgotPassword} />
        <Route exact path="/profile" component={SignIn} />
      </Switch>
    </ScrollToTop>
  </HashRouter>
);
