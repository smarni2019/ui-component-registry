import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class ForgotPassword extends React.Component {
  
  state = {
    open: false,
    formSubmitted: false,
    dialogContentText: "Please enter your email address here."
  };

  handleClickOpen = () => {
    this.setState({
      open: true,
      formSubmitted: false
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
      formSubmitted: false,
      dialogContentText: "Please enter your email address here."
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      formSubmitted: true,
      dialogContentText: "A new password has been sent to your email address if a corresponding profile was found."
    });
  };

  render() {
    const formSubmitted = this.state.formSubmitted;
    return (
      <div>
        <Button
          variant="text"
          color="primary"
          onClick = {this.handleClickOpen}
        >
          Forgot Password
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form onSubmit={this.handleSubmit} >
            <DialogTitle id="form-dialog-title" > Forgot Password </DialogTitle>
            <DialogContent>
              <DialogContentText>{this.state.dialogContentText} </DialogContentText>
              {
                !formSubmitted ? (
                  <FormControl margin="normal" required fullWidth >
                    <InputLabel htmlFor="email" > Email Address </InputLabel>
                    <Input
                      id="email"
                      name="email"
                      autoComplete="email"
                      autoFocus
                    />
                  </FormControl>
                ) : null
              }
            </DialogContent>
            <DialogActions>
              {
                !formSubmitted ? (
                  <Fragment>
                    <Button onClick={this.handleClose} color="primary">
                      Cancel
                    </Button>
                    <Button type="submit" color="primary">
                      Submit
                    </Button>
                  </Fragment>
                ) : (
                  <Button onClick={this.handleClose} color="primary">
                    Back to Login
                  </Button>
                )
              }
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}